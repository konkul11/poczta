﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Poczta
{
    public abstract class WiadomoscMail : Wiadomosc
    {

        public string Temat { get; set; }

        public Attachment Zalacznik { get; set; }


        protected WiadomoscMail(string tresc, List<string> odbiorcy, string temat, Attachment attachment)
             : base(tresc, odbiorcy)
        {
            Temat = temat;
            Zalacznik = attachment;
        }

        public override bool SprawdzOdbiorce(string daneOdbiorcy)
        {
            try
            {
                MailAddress m = new MailAddress(daneOdbiorcy);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public override bool Wyslij()
        {
            try
            {
                var fromAddress = new MailAddress("testkonkul@gmail.com", "Konrad Kulikowski");
                string fromPassword = "maselkomaselko";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };

                var message = new MailMessage()
                {
                    From = fromAddress,
                    Subject = Temat,
                    Body = Tresc
                };

                if (Zalacznik != null)
                {
                    message.Attachments.Add(Zalacznik);
                }

                foreach (string odbiorca in ListaOdbiorcow)
                {
                    message.To.Add(odbiorca);
                }

                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

}


