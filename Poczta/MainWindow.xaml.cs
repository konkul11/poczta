﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Poczta
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btWyslijWiadomosc_Click(object sender, RoutedEventArgs e)
        {
            List<string> listaOdbiorcow = new List<string>();

            if (tbOdbiorcaPierwszy.Text != "")
            {
                listaOdbiorcow.Add(tbOdbiorcaPierwszy.Text);
            }
            if (tbodbiorcaDrugi.Text != "")
            {
                listaOdbiorcow.Add(tbodbiorcaDrugi.Text);
            }
            if (tbOdbiorcaTrzeci.Text != "")
            {
                listaOdbiorcow.Add(tbOdbiorcaTrzeci.Text);
            }

            // Wiadomosc w= kjhsdkjhsdf.stworszkonkretnawiadomosc(...)
            //rezultat = w.wyslij()
            if (listaOdbiorcow.Any())
            {
                Wiadomosc wiadomosc = Wiadomosc.StworzKonkretnaWiadomosc(cbTypWiadomosci.Text, tbTresc.Text, listaOdbiorcow, cbMedium.Text);

                bool rezultatSprawdzeniaOdbiorcy = listaOdbiorcow.Where(x => wiadomosc.SprawdzOdbiorce(x) == false).Any();
                //foreach (string odbiorca in listaOdbiorcow)
                //{
                //    rezultatSprawdzeniaOdbiorcy = wiadomosc.SprawdzOdbiorce(odbiorca);
                //    if (rezultatSprawdzeniaOdbiorcy == false)
                //    {
                //        break;
                //    }
                //}
                if (rezultatSprawdzeniaOdbiorcy == true)
                {
                    wiadomosc.Wyslij();
                    MessageBox.Show("Wiadomosc Wyslana");
                }
                else
                {
                    MessageBox.Show("Nieprawidlowy odbiorca");
                }
            }
            else
            {
                MessageBox.Show("Brak odbiorcow");
            }
        }

        //    if (cbMedium.Text == "Mail")
        //    {
        //        if (listaOdbiorcow.Any())
        //        {
        //            bool czyWszyscyOdbiorcyPoprawni = !listaOdbiorcow.Where(x => !x.Contains('@')).Any();
        //            //foreach (string odbiorca in listaOdbiorcow)
        //            //{
        //            //    if (!odbiorca.Contains('@'))
        //            //    {
        //            //        MessageBox.Show("Nieprawidlowy odbiorca");
        //            //        czyWszyscyOdbiorcyPoprawni = false;
        //            //        break;
        //            //    }
        //            //}
        //            if (czyWszyscyOdbiorcyPoprawni == true)
        //            {
        //                //Wiadomosc wiadomosc = new WiadomoscUrodzinowa(tbTresc.Text, listaOdbiorcow);

        //                Wiadomosc wiadomosc = Wiadomosc.StworzKonkretnaWiadomosc(cbTypWiadomosci.Text, tbTresc.Text, listaOdbiorcow);
        //                wiadomosc.Wyslij();
        //                MessageBox.Show("Wiadomosc Wyslana");
        //            }
        //            else
        //            {
        //                MessageBox.Show("Nieprawidlowy odbiorca");
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Brak elementow na liscie");
        //        }
        //    }
        //    else if (cbMedium.Text == "SMS")

        //    {
        //        if (listaOdbiorcow.Any())
        //        {
        //            WiadomoscSMS wiadomoscSMS = new Wiadomosc(tbTresc.Text, listaOdbiorcow);
        //            bool rezultatWalidacji = true;

        //            foreach (string odbiorca in listaOdbiorcow)
        //            {
        //                rezultatWalidacji = wiadomoscSMS.SprawdzOdbiorce(odbiorca);
        //                if (rezultatWalidacji == false)
        //                {
        //                    break;
        //                }
        //            }

        //            if (rezultatWalidacji == true)
        //            {
        //                //wiadomoscSMS.Wyslij();
        //                MessageBox.Show("Wiadomosc Wyslana");
        //            }
        //            else
        //            {
        //                MessageBox.Show("Nieprawidlowy odbiorca");
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Brak elementow na liscie");
        //        }
        //    }
        //}
    }
}
