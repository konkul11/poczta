﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Windows;
using System.Net;
using static System.Net.Mime.MediaTypeNames;
using System.IO;
using System.Net.Mime;
namespace Poczta
{
    public abstract class Wiadomosc
    {
        public List<string> ListaOdbiorcow { get; set; }

        public string Tresc { get; set; }


        protected Wiadomosc(string tresc, List<string> odbiorcy)
        {
            ListaOdbiorcow = odbiorcy;
            Tresc = tresc;
        }


        public static Wiadomosc StworzKonkretnaWiadomosc(string typ, string tresc, List<string> listaOdbiorcow, string rodzajMedium)
        {
            if (rodzajMedium == "Mail")
            {
                switch (typ)
                {
                    case "Życzenia urodzinowe":
                        return new WiadomoscUrodzinowa(tresc, listaOdbiorcow);
                    case "Życzenia imieninowe":
                        return new WiadomoscImieninowa(tresc, listaOdbiorcow);
                    case "Raport":
                        return new WiadomoscRaport(tresc, listaOdbiorcow);
                    default:
                        return null;
                }
            }
            else
            {
                return new WiadomoscSMS(tresc, listaOdbiorcow);
            }

        }
        public abstract bool Wyslij();
        public abstract bool SprawdzOdbiorce(string daneOdbiorcy);
    }
}
