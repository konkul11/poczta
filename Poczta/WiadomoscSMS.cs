﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Poczta
{
    public class WiadomoscSMS : Wiadomosc
    {
        public WiadomoscSMS(string tresc, List<string> odbiorcy)
            : base(tresc, odbiorcy)
        {
        }

        public override bool SprawdzOdbiorce(string daneOdbiorcy)
        {
            try
            {
                if (string.IsNullOrEmpty(daneOdbiorcy))
                    return false;
                Regex regex = new Regex(@"^[0-9]{9}$"); //wyrazenie ktore sprawia ze mam same cyfry od 0 do 9 i ilosc cyfr to 9 czyli np 123456789
                return regex.IsMatch(daneOdbiorcy);

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public override bool Wyslij()
        {
            try
            {
                SMSApi.Api.Client client = new SMSApi.Api.Client("konkul11@gmail.com");
                client.SetPasswordHash("d8578edf8458ce06fbc5bb76a58c5ca4");

                var smsApi = new SMSApi.Api.SMSFactory(client);

                foreach (string odbiorca in ListaOdbiorcow)
                {
                    var result =
                       smsApi.ActionSend()
                           .SetSender("ECO") //Pole nadawcy lub typ wiadomość 'ECO', '2Way'
                           .SetText(Tresc)
                           .SetTo(odbiorca)
                       .Execute();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
